<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        img {
            height: 500px;
        }
    </style>
</head>
<body>
    <h1>JavaScript 2-13</h1>

    <img src="./shingeki_no_kyoujin.jpg" alt="" id="img1">
    <br>
    <br>
    <button onclick="changeSize('s')">Small</button>
    <button onclick="changeSize('m')">Medium</button>
    <button onclick="changeSize('l')">Large</button>

    <script>
        function changeSize(size){
            if(size == 's'){
                document.getElementById('img1').style.height = '100px';
            } else if(size == 'm'){
                document.getElementById('img1').style.height = '250px';
            } else if(size == 'l'){
                document.getElementById('img1').style.height = '500px';
            }
        }
    </script>
</body>
</html>