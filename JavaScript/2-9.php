<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .padding {
            padding: 40px;
        }
    </style>
</head>
<body>
    <h1>JavaScript 2-9</h1>
    <div class="padding">
        <p id="text">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
    </div>
    <div>
        <label for="">Background</label> <br>
        <button style="background-color: blue; color: white" onclick="changeColor('bg-blue')">blue</button>
        <button style="background-color: red; color: white" onclick="changeColor('bg-red')">Red</button>
        <button style="background-color: green; color: white " onclick="changeColor('bg-green')">green</button>
    </div>
    <div>
        <label for="">Font Color</label> <br>
        <button style="background-color: white; color: blue" onclick="changeColor('font-blue')">blue</button>
        <button style="background-color: white; color: red" onclick="changeColor('font-red')">Red</button>
        <button style="background-color: white; color: green" onclick="changeColor('font-green')">green</button>
    </div>

    <script>
        function changeColor(color){
            if(color == 'bg-blue'){
                document.getElementById('text').style.backgroundColor = 'blue';
            } else if(color == 'bg-red'){
                document.getElementById('text').style.backgroundColor = 'red';
            } else if(color == 'bg-green'){
                document.getElementById('text').style.backgroundColor = 'green';
            }

            if(color == 'font-blue'){
                document.getElementById('text').style.color = 'blue';
            } else if(color == 'font-red'){
                document.getElementById('text').style.color = 'red';
            } else if(color == 'font-green'){
                document.getElementById('text').style.color = 'green';
            }
        }
    </script>
</body>
</html>