<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>JavaScript 2-8</h1>
    <a href="https://www.fb.com" id="link">FaceBook</a>

    <script>
        document.getElementById('link').addEventListener('click' , function(event){
            event.preventDefault();
            var link = document.getElementById('link').href;
            alert(link);
        });
    </script>
</body>
</html>