<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>JavaScript 2-4</h1>
    <input type="number" id="num1">
    <button onclick="calculate()">Calculate</button>

    <p id="output"></p>

    <script>
        function calculate(){
            var lastNumber = parseInt(document.getElementById('num1').value);
            var i = 0;
            var num = 2;
            var primeNumbers = [];

            while(num <= lastNumber){
                var div_count = 0;
                for(i = 1; i <= num; i++){
                    console.log(i)
                    if(num % i == 0){
                        div_count++;
                    }
                }
                if(div_count < 3){
                    primeNumbers.push(num);
                }
                num++
            }
            document.getElementById('output').innerHTML = primeNumbers;
        }
    </script>
</body>
</html>