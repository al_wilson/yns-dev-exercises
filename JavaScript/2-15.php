<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>JavaScript 2-15</h1>
    <label for="" id="dateLabel"></label>

    <script>
        var dateTime = new Date();
        document.getElementById('dateLabel').innerHTML = dateTime.toLocaleString();

        setInterval(function() {
            var dateTime = new Date();
            document.getElementById('dateLabel').innerHTML = dateTime.toLocaleString();
            // console.log(dateTime.toLocaleString());

        }, 60 * 1000);
    </script>
</body>
</html>