<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        img {
            height: 500px;
        }
    </style>
</head>
<body>
    <h1>JavaScript 2-7</h1>
    <img src="../JavaScript/aldnoah_zero.png" alt="" id="img1" onclick="getFileName()">

    <script>
        function getFileName(){
            var filePath = document.getElementById('img1').src;
            filePath = filePath.split('/');
            filePath = filePath[filePath.length - 1];
            // console.log(filePath);
            alert(filePath);
        }

    </script>
</body>
</html>