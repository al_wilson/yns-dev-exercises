<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>JavaScript 2-3</h1>
    <input type="number" id="num1">
    <input type="number" id="num2">
    <select name="operation" id="operation">
        <option value="add">+ (addition)</option>
        <option value="sub">- (subtraction)</option>
        <option value="mult">* (multiplication)</option>
        <option value="div">/ (division)</option>
    </select>
    <button onclick="calculate()">Calculate!</button>

    <p id='output'></p>

    <script>
        function calculate(){
            var num1 = parseInt(document.getElementById('num1').value);
            var num2 = parseInt(document.getElementById('num2').value);
            var operation = document.getElementById('operation').value;
            if(operation === 'add'){
                var output = num1 + num2;
            } else if(operation === 'sub'){
                var output = num1 - num2;
            } else if(operation === 'mult'){
                var output = num1 * num2;
            } else if(operation === 'div'){
                var output = num1 / num2;
            }
            document.getElementById('output').innerHTML = "Output: " + output;
        }
    </script>
</body>
</html>