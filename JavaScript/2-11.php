<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .padding {
            padding: 40px;
        }
    </style>
</head>
<body>
    <h1>JavaScript 2-11</h1>

    <script>
        document.body.style.backgroundColor = '#000066';
        setTimeout(colorChange, 1000);

        function colorChange(){
            document.body.style.backgroundColor = '#b3b3ff';
        }
        
    </script>
</body>
</html>