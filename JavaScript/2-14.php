<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        img {
            height: 500px;
        }
    </style>
</head>
<body>
    <h1>JavaScript 2-14</h1>

    <img src="./aldnoah_zero.png" alt="" id="img1">
    <select id="imgList" onchange="changeImage()">
        <option value="1">Aldnoah</option>
        <option value="2">Shingeki No Kyoujin</option>
        <option value="3">Kimi No Nawa</option>
        <option value="4">Dr. Stone</option>
    </select>

    <script>
        function changeImage(){
            var img = document.getElementById('imgList').value;
            
            if(img == '1'){
                document.getElementById('img1').src = './aldnoah_zero.png'
            } else if(img == '2'){
                document.getElementById('img1').src = './shingeki_no_kyoujin.jpg'
            } else if(img == '3'){
                document.getElementById('img1').src = './kimi_no_nawa_morning.jpg'
            } else if(img == '4'){
                document.getElementById('img1').src = './dr_stone_senku.jpg'
            }
        }
    </script>
</body>
</html>