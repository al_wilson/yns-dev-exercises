<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>JavaScript 2-2</h1>
    <button onclick="showDialog()">Click Me!</button>

    <script>

        function showDialog(){
            if(confirm('Click ok to redirect to another page!')){
                window.location.href = "2-2_newPage.php"
            }
        }

    </script>
</body>
</html>