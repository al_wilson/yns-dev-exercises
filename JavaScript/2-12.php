<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        img {
            height: 550px;
        }
    </style>
</head>
<body>
    <h1>JavaScript 2-12</h1>
    <img src="./kimi_no_nawa_dawn.jpg" alt="" id="img1">

    <script>
        document.getElementById('img1').addEventListener('mouseover', function(event){
            this.src = './kimi_no_nawa_morning.jpg';
        });
        document.getElementById('img1').addEventListener('mouseout', function(event){
            this.src = './kimi_no_nawa_dawn.jpg';
        });
    </script>
</body>
</html>