<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .padding {
            padding: 40px;
        }
    </style>
</head>
<body>
    <h1>JavaScript 2-10</h1>

    <div>
        <button onclick="pageDown()">Go Down!</button>
    </div>

    <div class="padding">
        <p id="text">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
        Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
    </div>

    <script>
        function appendText(){
            var i = 0;
            while(i != 5){
                var text = document.getElementById('text').innerText;
                text = document.createTextNode(text);
                document.getElementById('text').appendChild(text);
                i++
            }

            var div = document.createElement('div');
            document.body.appendChild(div);
            div = div.setAttribute("id", "div1");
       
            var btn = document.createElement('button');
            const btn_label = document.createTextNode('Go up!');

            btn.appendChild(btn_label);
            document.getElementById('div1').appendChild(btn);
            btn = btn.setAttribute("id", "btn2");
        
        }
        appendText();

        function pageDown(){
            window.scrollTo(0,document.body.scrollHeight);
        }

        document.getElementById('btn2').addEventListener('click', function(event){
            console.log('here')
            window.scrollTo(0, 0);
        });
    </script>
</body>
</html>