<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>JavaScript 2-5</h1>
    <input type="text" id="txt1" oninput="typing()"> <br>
    <label for="" id="lbl1"></label>

    <script>
        function typing(){
            var output = document.getElementById('txt1').value;
            document.getElementById('lbl1').innerHTML = output;
        }
    </script>
</body>
</html>