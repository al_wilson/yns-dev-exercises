<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>JavaScript 2-6</h1>
    <button onclick="append()">Click Me!</button>

    <div id='output'>
    </div>

    <script>
        function append(){
            const label = document.createElement('label');
            const br = document.createElement('br');
            const label_text = document.createTextNode('new label');
            label.appendChild(label_text);

            document.getElementById('output').appendChild(label);
            document.getElementById('output').appendChild(br);
        }
    </script>
</body>
</html>