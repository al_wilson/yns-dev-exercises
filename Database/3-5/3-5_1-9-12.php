<?php 
include_once('./database.php');
session_start();
if(!isset($_SESSION['user'])){
    header("Location: 3-5_1-13.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        .navigation {
            display:inline-block;
            padding-left: 5px;
            padding-right: 5px;
            border: 1px solid black;

        }
    </style>
</head>
<body>
    <?php
        require_once '../../Practice Systems-Programs/6-3.php';
        $sql = "SELECT * FROM userInfo";
        if($conn){
            $result  = $conn->query($sql);
        }

        $data = [];
        while($row = $result->fetch_assoc()){
            $data[] = $row;
        }

        $totalRowCount = mysqli_num_rows($result);
        $itemPerPage = 10;
        $totalPages = ceil($totalRowCount / $itemPerPage);

        if(!isset($_GET['page'])){
            $page_number = 1;
        } else {
            $page_number = $_GET['page'];
        }

        $initialPage = ($page_number - 1) * $itemPerPage;
        $dataOnPage = array_slice($data, $initialPage, $itemPerPage);
    ?>
    <br>
    <table>
        <tr>
            <th>#</th>
            <th>Image</th>
            <th>First name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Date of Birth</th>
            <th>Gender</th>
            <th>E-Mail</th>
        </tr>
        <?php
            $scriptFile = explode('/', $_SERVER['SCRIPT_NAME']);
            $scriptFile = end($scriptFile);

            foreach($dataOnPage as $row){
                echo "<tr>";
                echo "<td>", $row['id'] ,"</td>";
                if(file_exists('userImages/' . $row['image'])){
                    echo "<td><img src=" . 'userImages/' . $row['image'] . " height=150px></td>";
                } 
                echo "<td>", $row['fName'] ,"</td>";
                echo "<td>", $row['lName'] ,"</td>";
                echo "<td>", $row['age'] ,"</td>";
                echo "<td>", $row['date_of_birth'] ,"</td>";
                echo "<td>", $row['gender'] ,"</td>";
                echo "<td>", $row['email'] ,"</td>";
                echo "<td>
                     <form method='post' action='uploadImage.php' enctype='multipart/form-data'>
                     <input type='file' name='userImage' value=''/>
                     <input type='hidden' name='user_id' value='" , $row['id'] ,"'/>
                     <input type='hidden' name='scriptFile' value='" , $scriptFile ,"'/>
                     <button type='submit' name='uploadfile'>Upload</button>
                     </form>
                    </td>";
                echo "</tr>";
            }
        ?>
    </table>
    <br>
    <?php
    for($page_number = 1; $page_number <= $totalPages; $page_number++){
        echo "<a href='3-5_1-9-12.php?page=" , $page_number , "' class='navigation'>" , $page_number , "</a>";
    }
    ?>
    <br>
    <br>
    <a href="logout.php"> <button> Logout </button> </a>
</body>
</html>