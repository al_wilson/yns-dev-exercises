<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    require_once '../../Practice Systems-Programs/6-3.php';
    echo "<h1>3-5 of 1-6,7,8</h1>";
    if(isset($_SESSION['message'])){
        echo "<ul>";
        if(count($_SESSION['message']) > 0){
            echo count($_SESSION['message']), " Error occur!", "<br>";
            foreach($_SESSION['message'] as $errorMsg){
                echo "<li>", $errorMsg, "</li>";
            }
        }
        echo "</ul>";
    }
    ?>

    <form name="form" action="insertData.php" method="post">
        <div class="margin: 50%">
            <label for="fName">First Name:</label> <br>
            <input name="fName" type=text value="<?php echo $_SESSION['fName'] ?? '' ?>"> <br>

            <label for="lName">Last Name:</label> <br>
            <input name="lName" type=text value="<?php echo $_SESSION['lName'] ?? '' ?>"> <br>
            
            <label for="age">Age:</label> <br>
            <input name="age" type=text value="<?php echo $_SESSION['age'] ?? '' ?>"> <br>
            
            <label for="dob">Date Of Birth:</label> <br>
            <input name="dob" type=date value="<?php echo $_SESSION['dob'] ?? '' ?>"> <br>

            <label for="gender">Gender:</label> <br>
            <input type='radio' name='gender' value='Male' <?php echo isset($_SESSION['gender']) && $_SESSION['gender'] == 'Male' ? 'checked' : '' ?>> Male <br>
            <input type='radio' name='gender' value='Female' <?php echo isset($_SESSION['gender']) && $_SESSION['gender'] == 'Female' ? 'checked' : '' ?>> Female <br>
            
            <label for="email">E-Mail Address:</label> <br>
            <input name="email" type="text" value="<?php echo $_SESSION['email'] ?? '' ?>"> <br>

            <label for="password">Password:</label> <br>
            <input name="password" type="password"> <br>

            <br>
            <button type="submit">Submit</button><br>
        </div>
    </form>
</body>
</html>