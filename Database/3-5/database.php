<?php

// $servername = "localhost";
$servername = "mysql-server";
$username = "root";
$password = "secret";
$dbname = "yns_dev_exercise";

$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

// echo "Connected successfully";

?>