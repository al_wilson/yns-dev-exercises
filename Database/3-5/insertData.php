<?php
session_start();
include_once './database.php';

$errorMsg = [];

if(!isset($_POST['fName']) || $_POST['fName'] == ''){
    $errorMsg[] = 'First Name is Required';
} else {
    $_SESSION['fName'] = $_POST['fName'];
}

if(!isset($_POST['lName']) || $_POST['lName'] == ''){
    $errorMsg[] = 'Last Name is Required';
} else {
    $_SESSION['lName'] = $_POST['lName'];
}

if(!isset($_POST['age']) || $_POST['age'] == ''){
    $errorMsg[] = 'Age is Required';
} else {
    $_SESSION['age'] = $_POST['age'];
}

if(!is_numeric($_POST['age'])){
    $errorMsg[] = 'Age Should be numeric';
} 

if(!isset($_POST['dob']) || $_POST['dob'] == ''){
    $errorMsg[] = 'Date of Birth is Required';
} else {
    $_SESSION['dob'] = $_POST['dob'];
}

if(strtotime($_POST['dob']) == false){
    $errorMsg[] = 'Date of Birth is invalid';
}

if(!isset($_POST['gender']) || $_POST['gender'] == ''){
    $errorMsg[] = 'Gender is Required';
} else {
    $_SESSION['gender'] = $_POST['gender'];
}

if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
    $errorMsg[] = 'Email Address Should be a valid address';
}

if(!isset($_POST['email']) || $_POST['email'] == ''){
    $errorMsg[] = 'Mail Address is Required';
} else {
    $_SESSION['email'] = $_POST['email'];
}

if(!isset($_POST['password']) || $_POST['password'] == ''){
    $errorMsg[] = 'Password is Required';
} else {
    $_SESSION['password'] = $_POST['password'];
}

$_SESSION['message'] = $errorMsg;
if(count($_SESSION['message']) > 0){
    header("Location: 3-5_1-6-7-8.php");
    exit();
} else {
    $sql = "INSERT INTO userInfo (fName, lName, age, date_of_birth, gender, email, password)
    VALUES (
        '" . $_POST['fName'] . "',
        '" . $_POST['lName'] . "',
        '" . $_POST['age'] . "',
        '" . $_POST['dob'] . "',
        '" . $_POST['gender'] . "',
        '" . $_POST['email'] . "',
        '" . password_hash($_POST['password'], PASSWORD_DEFAULT) . "'
        )";
        
        if($conn->query($sql)) {
            require_once '../../Practice Systems-Programs/6-3.php';
            echo "<h1>3-5 of 1-6,7,8</h1>";
            echo "New record created successfully", "<br> <br>";
            echo "<b>First Name:</b> ", $_POST['fName'], "<br>";
            echo "<b>Last Name:</b> ", $_POST['lName'], "<br>";
            echo "<b>Age:</b> ", $_POST['age'], "<br>";
            echo "<b>Date Of Birth:</b> ", $_POST['dob'], "<br>";
            echo "<b>Gender:</b> ", $_POST['gender'] ?? '', "<br>";
            echo "<b>email:</b> ", $_POST['email'] ?? '', "<br>";
        } else {
            echo "Error: " . $sql . "<br>" . $conn->error;
        }
        $conn->close();
    }
?>