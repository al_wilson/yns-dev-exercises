<?php
include_once('./database.php');
session_start();

$errorMsg = [];
if(!isset($_POST['email']) || $_POST['email'] == ''){
    $errorMsg[] = 'Please type your email';
}
if(!isset($_POST['password']) || $_POST['password'] == ''){
    $errorMsg[] = 'Please type your password';
}

if(count($errorMsg) > 0){
    $_SESSION['message'] = $errorMsg;
    header("Location: 3-5_1-13.php");
    exit();
} else {
    $sql = "SELECT * FROM userInfo WHERE email = '".$_POST['email']."'";
    $result = $conn->query($sql);

    if(mysqli_num_rows($result) > 0){
        $data = $result->fetch_assoc();
        if(password_verify($_POST['password'], $data['password'])){
            $_SESSION['user'] = $data;
            header("Location: 3-5_1-9-12.php");
        } 
    } else {
        $errorMsg[] = 'No Record Found. Please check if your email and password are correct.';
        $_SESSION['message'] = $errorMsg;
        header("Location: 3-5_1-13.php");
        exit();
    }
}
?>