<?php
require_once('database.php');
if(isset($_FILES['userImage'])){
    $errors= array();
    $id = $_POST['user_id'];
    $file_name = $_FILES['userImage']['name'];
    $file_size = $_FILES['userImage']['size'];
    $file_tmp = $_FILES['userImage']['tmp_name'];
    $file_type = $_FILES['userImage']['type'];
    $tmp = explode('.', $_FILES['userImage']['name']);
    $file_ext = strtolower(end($tmp));
    
    $expensions= array("jpeg","jpg","png");
    
    if(in_array($file_ext,$expensions) == false){
        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    }
    
    if($file_size > 2097152){
        $errors[]='File size must be excately 2 MB';
    }
  
    if(empty($errors) == true){
        if(!file_exists("userImages/")){
            mkdir("userImages/");
        }

        if(file_exists('userImages/' . $file_name)){
            unlink('userImages/' . $file_name);
        } elseif(file_exists('userImages/' . $file_name)){
            unlink('userImages/' . $file_name);
        } elseif(file_exists('userImages/' . $file_name)) {
            unlink('userImages/' . $file_name);
        }

        $sql = "UPDATE userInfo SET image = '".$file_name."' WHERE id = $id";
        if($conn->query($sql)){
            move_uploaded_file($file_tmp,"userImages/".$file_name);
            header("Location: 3-5_1-9-12.php");
            exit();
        } else {

        }
      
    }else{
        foreach($errors as $error){
            echo "<li>" , $error , "</li>";
        }
    }
}
?>