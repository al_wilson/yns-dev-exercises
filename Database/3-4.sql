-- create employees table
CREATE TABLE IF NOT EXISTS employees (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    middle_name VARCHAR(255),
    birth_date DATE,
    department_id INTEGER NOT NULL,
    hire_date DATE,
    boss_id INTEGER
);

-- insert data in employees table
INSERT INTO `employees`
(`first_name`,`last_name`,`middle_name`,`birth_date`,`department_id`,`hire_date`,`boss_id`)
VALUES
("Manabu","Yamazaki",null,"1976-03-15",1,null,null),
("Tomohiko","Takasago",null,"1974-05-24",3,"2014-04-01",1),
("Yuta","Kawakami",null,"1990-08-13",4,"2014-04-01",1),
("Shogo","Kubota",null,"1985-01-31",4,"2014-12-01",1),
("Lorraine","San Jose","P.","1983-10-11",2,"2015-03-10",1),
("Haille","Dela Cruz","A.","1990-11-12",3,"2015-02-15",2),
("Godfrey","Sarmenta","L.","1993-09-13",4,"2015-01-01",1),
("Alex","Amistad","F.","1988-04-14",4,"2015-04-10",1),
("Hideshi","Ogoshi",null,"1983-07-15",4,"2014-06-01",1),
("Kim","","","1977-10-16",5,"2015-08-06",1);

-- create departments table
CREATE TABLE IF NOT EXISTS departments (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255)
);

-- insert data in employees table
INSERT INTO `departments` (`name`)
VALUES
("Executive"),
("Admin"),
("Sales"),
("Development"),
("Design"),
("Marketing");

-- create positions table
CREATE TABLE IF NOT EXISTS positions (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255)
);

-- insert data in positions table
INSERT INTO `positions` (`name`)
VALUES
("CEO"),
("CTO"),
("CFO"),
("Manager"),
("Staff");

-- create employee_positions table
CREATE TABLE IF NOT EXISTS employee_positions (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    employee_id INTEGER,
    position_id INTEGER,
    FOREIGN KEY (employee_id) REFERENCES `employees`(id),
    FOREIGN KEY (position_id) REFERENCES `positions`(id)
);

-- insert data in employee_positions table
INSERT INTO `employee_positions` (`employee_id`,`position_id`)
VALUES
(1,1),
(1,2),
(1,3),
(2,4),
(3,5),
(4,5),
(5,5),
(6,5),
(7,5),
(8,5),
(9,5),
(10	,5);

-- Exercise SELECT statement
-- 1) Retrieve employees whose last name start with "K".
SELECT * FROM `employees` WHERE `last_name` LIKE 'K%';
-- 2) Retrieve employees whose last name end with "i".
SELECT * FROM `employees` WHERE `last_name` LIKE '%I';
-- 3) Retrieve employee's full name and their hire date whose hire date is between 2015/1/1 and 2015/3/31 ordered by ascending by hire date.
SELECT CONCAT(`first_name`," ",`middle_name`," ", `first_name`) AS full_name, `hire_date`
FROM `employees`
WHERE `hire_date` BETWEEN "2015/1/1" AND "2015/3/31"
ORDER BY `hire_date` ASC;
-- 4) Retrieve employee's last name and their boss's last name. If they don't have boss, no need to retrieve and show.
SELECT Employee.last_name AS Employee, Boss.last_name AS Boss
FROM employees Employee, employees Boss
WHERE Employee.boss_id IS NOT NULL
AND Employee.boss_id = Boss.id;

-- 5) Retrieve employee's last name who belong to Sales department ordered by descending by last name.
SELECT `last_name`
FROM `employees`
WHERE `department_id` = 3
ORDER BY `last_name` DESC;

-- 6) Retrieve number of employee who has middle name.
SELECT COUNT(`middle_name`) AS count_has_middle
FROM `employees`
WHERE `middle_name` IS NOT NULL AND `middle_name` != '';

-- 7) Retrieve department name and number of employee in each department. You don't need to retrieve the department name which doesn't have employee.
SELECT departments.name AS Name, COUNT(departments.id)
FROM `departments`
INNER JOIN `employees`
ON departments.id = employees.department_id
GROUP BY `departments`.`name`;

-- 8) Retrieve employee's full name and hire date who was hired the most recently.
SELECT `first_name`, `middle_name`, `last_name`, `hire_date`
FROM `employees`
ORDER BY `hire_date` DESC LIMIT 1;

-- 9) Retrieve department name which has no employee.
SELECT `name`, `id`
FROM `departments`
WHERE `id` NOT IN (SELECT `department_id` FROM `employees`);

-- 10) Retrieve employee's full name who has more than 2 positions.
SELECT employees.first_name, employees.middle_name, employees.last_name
FROM `employees`
INNER JOIN employee_positions
ON employees.id = employee_positions.employee_id
HAVING COUNT(employee_positions.id > 2);