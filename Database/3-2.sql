CREATE DATABASE IF NOT EXISTS yns_dev_exercise;

CREATE TABLE IF NOT EXISTS User (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    lName varchar(255),
    fName varchar(255),
    age int,
    gender varchar(255)
);