<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>1-6</h1>
    <p>
        <?php
            echo "<b>First Name:</b> ", $_POST['fName'], "<br>";
            echo "<b>Last Name:</b> ", $_POST['lName'], "<br>";
            echo "<b>Age:</b> ", $_POST['age'], "<br>";
            echo "<b>Date Of Birth:</b> ", $_POST['dob'], "<br>";
            echo "<b>Gender:</b> ", $_POST['gender'] ?? '', "<br>";
        ?>
    </p>
</body>
</html>