<?php
session_start();
if(!isset($_SESSION['user'])){
    header("Location: 1-13.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        }
    </style>
</head>
<body>
    <?php require_once '../Practice Systems-Programs/6-3.php'; ?>
    <br>
    <table class="table">
        <tr>
            <th>#</th>
            <th>Image</th>
            <th>First name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Date of Birth</th>
            <th>Gender</th>
            <th>E-Mail</th>
            <th>Action</th>
        </tr>
        <?php
            $scriptFile = explode('/', $_SERVER['SCRIPT_NAME']);
            $scriptFile = end($scriptFile);

            $csvFile = fopen("userInfo.csv", "r");
            if($csvFile){
                while(($datas = fgetcsv($csvFile, 1000, ",")) != false){
                    $rowDataCount = count($datas);
                    echo "<tr>";
                    echo "<td>", $datas[0] ,"</td>";
                    if(file_exists('userImages/' . $datas[0] . '.jpeg')){
                        echo "<td><img src=" , 'userImages/' , $datas[0] , '.jpeg' ," height=150px></td>";
                    } elseif(file_exists('userImages/' . $datas[0] . '.jpg')){
                        echo "<td><img src=" , 'userImages/' , $datas[0] , '.jpg' ," height=150px></td>";
                    } else {
                        echo "<td><img src=" , 'userImages/' , $datas[0] , '.png' ," height=150px></td>";
                    }
                    echo "<td>", $datas[1] ,"</td>";
                    echo "<td>", $datas[2] ,"</td>";
                    echo "<td>", $datas[3] ,"</td>";
                    echo "<td>", $datas[4] ,"</td>";
                    echo "<td>", $datas[5] ,"</td>";
                    echo "<td>", $datas[6] ,"</td>";
                    echo "<td>
                     <form method='post' action='1-10_and_11_2.php' enctype='multipart/form-data'>
                     <input type='file' name='userImage' value=''/>
                     <input type='hidden' name='user_id' value='" , $datas[0] ,"'/>
                     <input type='hidden' name='scriptFile' value='" , $scriptFile ,"'/>
                     <button type='submit' name='uploadfile'>Upload</button>
                     </form>
                    </td>";
                    echo "</tr>";
                }
                fclose($csvFile);
            }
        ?>
    </table> <br>
    <a href="logout.php"> <button> Logout </button> </a>
</body>
</html>