<?php
session_start();
if(!isset($_SESSION['user'])){
    header("Location: 1-13.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php require_once '../Practice Systems-Programs/6-3.php'; ?>
    <h1>1-6</h1>
    <form name="form" action="1-6_2.php" method="post">
        <div class="margin: 50%">
            <label for="fName">First Name:</label> <br>
            <input name="fName" type=text> <br>

            <label for="lName">Last Name:</label> <br>
            <input name="lName" type=text> <br>
            
            <label for="age">Age:</label> <br>
            <input name="age" type=text> <br>
            
            <label for="dob">Date Of Birth:</label> <br>
            <input name="dob" type=date> <br>

            <label for="gender">Gender:</label> <br>    
            <input type="radio" name="gender" value="Male"> Male <br>
            <input type="radio" name="gender" value="Female"> Female <br>

            <br>
            <button type="submit">Submit</button><br>
        </div>
    </form> <br>
    <a href="logout.php"> <button> Logout </button> </a>
</body>
</html>