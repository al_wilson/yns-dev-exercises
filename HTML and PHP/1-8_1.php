<?php
session_start();
require_once '../Practice Systems-Programs/6-3.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>1-8</h1>
    <?php
        if(isset($_SESSION['message'])){
            echo "<ul>";
            if(count($_SESSION['message']) > 0){
                echo count($_SESSION['message']), " Error occur!", "<br>";
                foreach($_SESSION['message'] as $errorMsg){
                    echo "<li>", $errorMsg, "</li>";
                }
            }
            echo "</ul>";
        }
    ?>
    <form name="form" action="1-8_2.php" method="post">
        <div class="margin: 50%">
            <label for="fName">First Name:</label> <br>
            <input name="fName" type=text> <br>

            <label for="lName">Last Name:</label> <br>
            <input name="lName" type=text> <br>
            
            <label for="age">Age:</label> <br>
            <input name="age" type=text> <br>
            
            <label for="dob">Date Of Birth:</label> <br>
            <input name="dob" type=date> <br>

            <label for="gender">Gender:</label> <br>    
            <input type="radio" name="gender" value="Male"> Male <br>
            <input type="radio" name="gender" value="Female"> Female <br>

            <label for="email">E-Mail Address:</label> <br>
            <input name="email" type="text"> <br>

            <label for="password">Password:</label> <br>
            <input name="password" type="password"> <br>

            <br>
            <button type="submit">Submit</button><br>
        </div>
    </form>
</body>
</html>