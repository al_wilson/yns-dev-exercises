<?php
if(isset($_FILES['userImage'])){
    $errors= array();
    $id = $_POST['user_id'];
    $file_name = $_FILES['userImage']['name'];
    $file_size = $_FILES['userImage']['size'];
    $file_tmp = $_FILES['userImage']['tmp_name'];
    $file_type = $_FILES['userImage']['type'];
    $tmp = explode('.', $_FILES['userImage']['name']);
    $file_ext = strtolower(end($tmp));
    $file_name_new = $id . "." . $file_ext;
    
    $expensions= array("jpeg","jpg","png");
    
    if(in_array($file_ext,$expensions) == false){
        $errors[]="extension not allowed, please choose a JPEG or PNG file.";
    }
    
    if($file_size > 2097152){
        $errors[]='File size must be excately 2 MB';
    }
  
    if(empty($errors) == true){
        if(!file_exists("userImages/")){
            mkdir("userImages/");
        }
        if(file_exists('userImages/' . $id . '.jpeg')){
            unlink('userImages/' . $id . '.jpeg');
        } elseif(file_exists('userImages/' . $id . '.jpg')){
            unlink('userImages/' . $id . '.jpg');
        } elseif(file_exists('userImages/' . $id . '.png')) {
            unlink('userImages/' . $id . '.png');
        }
        move_uploaded_file($file_tmp,"userImages/".$file_name_new);
        if($_POST['scriptFile'] == "1-10_and_11_1.php"){
            header("Location: 1-10_and_11_1.php");
        } else {
            header("Location: 1-12.php");
        }
        exit();
    }else{
        foreach($errors as $error){
            echo "<li>" , $error , "</li>";
        }
    }
}
?>