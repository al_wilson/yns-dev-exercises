<?php
session_start();

$csvFile = fopen("userInfo.csv", "r");
$data = [];
if($csvFile){
    while(($datas = fgetcsv($csvFile, 1000, ",")) != false){
        $data[] = $datas;
    }
    fclose($csvFile);
}

$errorMsg = [];
if(!isset($_POST['email']) || $_POST['email'] == ''){
    $errorMsg[] = 'Please type your email';
}
if(!isset($_POST['password']) || $_POST['password'] == ''){
    $errorMsg[] = 'Please type your password';
}

$userInfo = [];
foreach($data as $row){
    if($row[6] === $_POST['email'] && $row[7] === $_POST['password']){
        $userInfo[] = $row;
        break;
    }
}

if(count($userInfo) > 0){
    $_SESSION['user'] = $userInfo[0];
    header("Location: 1-12.php");
    exit();
} else {
    $errorMsg[] = 'No Record Found. Please check if your email and password are correct.';
}

if(count($errorMsg) > 0){
    $_SESSION['message'] = $errorMsg;
    header("Location: 1-13.php");
    exit();
}
?>