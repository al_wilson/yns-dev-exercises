<?php
session_start();

$errorMsg = [];
if(!isset($_POST['fName']) || $_POST['fName'] == ''){
    $errorMsg[] = 'First Name is Required';
} 
if(!isset($_POST['lName']) || $_POST['lName'] == ''){
    $errorMsg[] = 'Last Name is Required';
} 
if(!isset($_POST['age']) || $_POST['age'] == ''){
    $errorMsg[] = 'Age is Required';
} 
if(!is_numeric($_POST['age'])){
    $errorMsg[] = 'Age Should be numeric';
} 
if(!isset($_POST['dob']) || $_POST['dob'] == ''){
    $errorMsg[] = 'Date of Birth is Required';
} 
if(strtotime($_POST['dob']) == false){
    $errorMsg[] = 'Date of Birth is invalid';
} 
if(!isset($_POST['gender']) || $_POST['gender'] == ''){
    $errorMsg[] = 'Gender is Required';
} 
if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
    $errorMsg[] = 'Email Address Should be a valid address';
} 
if(!isset($_POST['email']) || $_POST['email'] == ''){
    $errorMsg[] = 'Mail Address is Required';
}
if(!isset($_POST['password']) || $_POST['password'] = ''){
    $errorMsg[] = 'Password is Required';
}

if(count($errorMsg) > 0){
    $_SESSION['message'] = $errorMsg;
    header("Location: 1-7_1.php");
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php require_once '../Practice Systems-Programs/6-3.php'; ?>
    <h1>1-7</h1>
    <p>
        <?php            
            echo "<b>First Name:</b> ", $_POST['fName'], "<br>";
            echo "<b>Last Name:</b> ", $_POST['lName'], "<br>";
            echo "<b>Age:</b> ", $_POST['age'], "<br>";
            echo "<b>Date Of Birth:</b> ", $_POST['dob'], "<br>";
            echo "<b>Gender:</b> ", $_POST['gender'] ?? '', "<br>";
            echo "<b>email:</b> ", $_POST['email'] ?? '', "<br>";
        ?>
    </p>
</body>
</html>