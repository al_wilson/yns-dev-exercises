<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>1-3</h1>

    <form name="form" action="1-3.php" method="get">
        <input name="firstNumber" type=text value="<?php echo isset($_GET['firstNumber']) ? $_GET['firstNumber'] : '' ?>">
        <input name="secondNumber" type=text value="<?php echo isset($_GET['secondNumber']) ? $_GET['secondNumber'] : '' ?>">
        <button type="submit">Calculate</button><br>
    </form>

    <p>
        <?php
            if(isset($_GET['firstNumber']) && isset($_GET['secondNumber'])){
                if(is_numeric($_GET['firstNumber']) && is_numeric($_GET['secondNumber'])){
                    $firstNumber = $_GET['firstNumber'];
                    $secondNumber = $_GET['secondNumber'];

                    while($firstNumber != $secondNumber){
                        if($firstNumber > $secondNumber){
                            $firstNumber = $firstNumber - $secondNumber;
                            $gcd = $firstNumber;
                        } else {
                            $secondNumber = $secondNumber - $firstNumber;
                            $gcd = $firstNumber;
                        }
                    }
                    echo "Greatest Common Devisor: " . $gcd . "<br>";
                }
            }
        ?>
    </p>
</body>
</html>