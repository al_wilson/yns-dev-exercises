<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>1-4</h1>

    <form name="form" action="1-4.php" method="get">
        <input name="number" type=text value="<?php echo isset($_GET['number']) ? $_GET['number'] : '' ?>">
        <button type="submit">Calculate</button><br>
    </form>

    <p>
        <?php
        if(isset($_GET['number'])){
            if(is_numeric($_GET['number'])){
                $x = $_GET['number'];
                $i = 0;
                while($i != $x){
                    $i++;
                    if($i % 3 === 0 && $i % 5 === 0){
                        echo "FizzBuzz" . "<br>";
                    } elseif($i % 3 === 0){
                        echo "Fizz" . "<br>";
                    } elseif($i % 5 === 0){
                        echo "Buzz" . "<br>";
                    } else {
                        echo $i . "<br>";
                    }
                }
            }
        }  
        ?>
    </p>
</body>
</html>