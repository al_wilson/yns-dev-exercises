<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>1-2</h1>
    <form name="form" action="1-2.php" method="get">
        <input name="firstNumber" type=text value="<?php echo isset($_GET['firstNumber']) ? $_GET['firstNumber'] : '' ?>">
        <input name="secondNumber" type=text value="<?php echo isset($_GET['secondNumber']) ? $_GET['secondNumber'] : '' ?>">
        <button type="submit">Calculate</button><br>
    </form>

    <p>
        <?php
            if(isset($_GET['firstNumber']) && isset($_GET['secondNumber'])){
                if(is_numeric($_GET['firstNumber']) && is_numeric($_GET['secondNumber'])){
                    $addition = $_GET['firstNumber'] + $_GET['secondNumber'];
                    $substraction = $_GET['firstNumber'] - $_GET['secondNumber'];
                    $multiplication = $_GET['firstNumber'] * $_GET['secondNumber'];
                    $division = $_GET['firstNumber'] / $_GET['secondNumber'];

                    echo "Addition:" . $addition . "<br>";
                    echo "Substraction:" . $substraction . "<br>";
                    echo "Multiplication:" . $multiplication . "<br>";
                    echo "Division:" . $division . "<br>";
                }
            }
        ?>
    </p>
</body>
</html>