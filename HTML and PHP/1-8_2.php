<?php
session_start();
$errorMsg = [];
if(!isset($_POST['fName']) || $_POST['fName'] == ''){
    $errorMsg[] = 'First Name is Required';
} 
if(!isset($_POST['lName']) || $_POST['lName'] == ''){
    $errorMsg[] = 'Last Name is Required';
} 
if(!isset($_POST['age']) || $_POST['age'] == ''){
    $errorMsg[] = 'Age is Required';
} 
if(!is_numeric($_POST['age'])){
    $errorMsg[] = 'Age Should be numeric';
} 
if(!isset($_POST['dob']) || $_POST['dob'] == ''){
    $errorMsg[] = 'Date of Birth is Required';
} 
if(strtotime($_POST['dob']) == false){
    $errorMsg[] = 'Date of Birth is invalid';
} 
if(!isset($_POST['gender']) || $_POST['gender'] == ''){
    $errorMsg[] = 'Gender is Required';
} 
if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
    $errorMsg[] = 'Email Address Should be a valid address';
} 
if(!isset($_POST['email']) || $_POST['email'] == ''){
    $errorMsg[] = 'Mail Address is Required';
}
if(!isset($_POST['password']) || $_POST['password'] = ''){
    $errorMsg[] = 'Password is Required';
}

$_SESSION['message'] = $errorMsg;
if(count($_SESSION['message']) > 0){
    header("Location: 1-8_1.php");
    exit();
} else {
    $csvFile = fopen("userInfo.csv", "a");
    $fileRows = count(file("userInfo.csv"));
    $_REQUEST = array("id" => $fileRows + 1) + $_REQUEST;
    fputcsv($csvFile, $_REQUEST);
    fclose($csvFile);
    
    require_once '../Practice Systems-Programs/6-3.php';
    echo "<h1>1-8</h1>";
    echo "Data has been store to csv file.", "<br> <br>";
    echo "<b>First Name:</b> ", $_POST['fName'], "<br>";
    echo "<b>Last Name:</b> ", $_POST['lName'], "<br>";
    echo "<b>Age:</b> ", $_POST['age'], "<br>";
    echo "<b>Date Of Birth:</b> ", $_POST['dob'], "<br>";
    echo "<b>Gender:</b> ", $_POST['gender'] ?? '', "<br>";
    echo "<b>email:</b> ", $_POST['email'] ?? '', "<br>";
}
?>