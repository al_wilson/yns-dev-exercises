<?php require_once '../Practice Systems-Programs/6-3.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>1-5</h1>

    <form name="form" action="1-5.php" method="get">
        <input name="date" type=date value="<?php echo isset($_GET['date']) ? $_GET['date'] : '' ?>">
        <button type="submit">Calculate</button><br>
    </form>

    <p>
        <?php
        if(isset($_GET['date']) && $_GET['date'] != ''){
            $date = $_GET['date'];
               $i = 0;
               while($i != 3){
                   $i++;
                   $newDate = new DateTime($date);
                   $newDate->add(new DateInterval('P' . $i . 'D'));
                   echo $newDate->format('d-m-Y D'), "<br>";
               }
        }  
        ?>
    </p>
</body>
</html>