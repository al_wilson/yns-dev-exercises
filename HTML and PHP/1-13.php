<?php
session_start();
require_once '../Practice Systems-Programs/6-3.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        if(isset($_SESSION['message'])){
            echo "<ul>";
            if(count($_SESSION['message']) > 0){
                echo count($_SESSION['message']), " Error occur!", "<br>";
                foreach($_SESSION['message'] as $errorMsg){
                    echo "<li>", $errorMsg, "</li>";
                }
            }
            echo "</ul>";
        }
        session_destroy();
    ?>
    <div>
        <h1>LogIn</h1>
        <form action="loginValidation.php" method="post">
            <label for=email>Email:</label>
            <input type="mail" name="email">

            <label for="password">Password</label>
            <input type="password" name="password">

            <input type="submit" name="Submit">
            <a href="1-8_1.php">Register</a>
        </form>
    </div>
</body>
</html>