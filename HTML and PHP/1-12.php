<?php
session_start();
if(!isset($_SESSION['user'])){
    header("Location: 1-13.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        table, th, td {
            border: 1px solid black;
            border-collapse: collapse;
        }
        .navigation {
            display:inline-block;
            padding-left: 5px;
            padding-right: 5px;
            border: 1px solid black;

        }
    </style>
</head>
<body>
    <?php require_once '../Practice Systems-Programs/6-3.php'; ?>
    <h1>1-12</h1>
    <?php
        $csvFile = fopen("userInfo.csv", "r");
        $data = [];
        if($csvFile){
            while(($datas = fgetcsv($csvFile, 1000, ",")) != false){
                $data[] = $datas;
            }
            fclose($csvFile);
        }

        $totalRowCount = count($data);
        $itemPerPage = 10;
        $totalPages = ceil($totalRowCount / $itemPerPage);

        if(!isset($_GET['page'])){
            $page_number = 1;
        } else {
            $page_number = $_GET['page'];
        }

        $initialPage = ($page_number - 1) * $itemPerPage;
        $dataOnPage = array_slice($data, $initialPage, $itemPerPage);
    ?>

    <table class="table">
        <tr>
            <th>#</th>
            <th>Image</th>
            <th>First name</th>
            <th>Last Name</th>
            <th>Age</th>
            <th>Date of Birth</th>
            <th>Gender</th>
            <th>E-Mail</th>
        </tr>
        <?php
            $scriptFile = explode('/', $_SERVER['SCRIPT_NAME']);
            $scriptFile = end($scriptFile);

            foreach($dataOnPage as $row){
                echo "<tr>";
                echo "<td>", $row[0] ,"</td>";
                if(file_exists('userImages/' . $row[0] . '.jpeg')){
                    echo "<td><img src=" , 'userImages/' , $row[0] , '.jpeg' ," height=150px></td>";
                } elseif(file_exists('userImages/' . $row[0] . '.jpg')){
                    echo "<td><img src=" , 'userImages/' , $row[0] , '.jpg' ," height=150px></td>";
                } else {
                    echo "<td><img src=" , 'userImages/' , $row[0] , '.png' ," height=150px></td>";
                }
                echo "<td>", $row[1] ,"</td>";
                echo "<td>", $row[2] ,"</td>";
                echo "<td>", $row[3] ,"</td>";
                echo "<td>", $row[4] ,"</td>";
                echo "<td>", $row[5] ,"</td>";
                echo "<td>", $row[6] ,"</td>";
                echo "<td>
                     <form method='post' action='1-10_and_11_2.php' enctype='multipart/form-data'>
                     <input type='file' name='userImage' value=''/>
                     <input type='hidden' name='user_id' value='" , $row[0] ,"'/>
                     <input type='hidden' name='scriptFile' value='" , $scriptFile ,"'/>
                     <button type='submit' name='uploadfile'>Upload</button>
                     </form>
                    </td>";
                echo "</tr>";
            }
        ?>
    </table>
    <br>
    <?php
    for($page_number = 1; $page_number <= $totalPages; $page_number++){
        echo "<a href='1-12.php?page=" , $page_number , "' class='navigation'>" , $page_number , "</a>";
    }
    ?>
    <br>
    <br>
    <a href="logout.php"> <button> Logout </button> </a>
</body>
</html>