SELECT CONCAT(COALESCE(`last_name`, ""), ", ", COALESCE(`first_name`, ""), " ", COALESCE(`middle_name`, ""), ".") AS `Full Name`, employees.birth_date,employees.department_id,employees.hire_date
(CASE
	WHEN positions.id = 1 THEN 'Chief Executive Officer'
	WHEN positions.id = 2 THEN 'Chief Technical Officer'
	WHEN positions.id = 3 THEN 'Chief Financial Officer'
    ELSE positions.name
END) AS `Position`
FROM `employees`
INNER JOIN `employee_positions` ON employees.id = employee_positions.employee_id
INNER JOIN `positions` ON employee_positions.position_id = positions.id;