CREATE TABLE IF NOT EXISTS master_data (
    id INTEGER,
    parent_id INTEGER
);

INSERT INTO master_data (`id`,`parent_id`)
VALUES
(1,null),
(2,5),
(3,null),
(4,1),
(5,null),
(6,3),
(7,3),
(null,null);

SELECT `id`, `parent_id`
FROM (
    SELECT id, parent_id, COALESCE(parent_id, id) AS new_parent_id
    FROM `master_data`
    WHERE id IS NOT NULL
) result
ORDER BY new_parent_id ASC