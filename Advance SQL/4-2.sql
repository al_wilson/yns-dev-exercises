-- A
CREATE TABLE IF NOT EXISTS therapists (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS daily_work_shift (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    therapist_id INTEGER NOT NULL,
    target_date DATE,
    start_time TIME,
    end_time TIME,
    FOREIGN KEY (therapist_id) REFERENCES therapists(id)
);

-- B
-- 1
INSERT INTO therapists (`name`) VALUES ('John');
INSERT INTO daily_work_shift (`therapist_id`, `target_date`, `start_time`, `end_time`) VALUES (1,'2022-03-11','14:00:00', '15:00:00');
-- 2
INSERT INTO therapists (`name`) VALUES ('Arnold');
INSERT INTO daily_work_shift (`therapist_id`, `start_time`, `end_time`) VALUES (2,'2022-03-11','22:00:00', '23:00:00');
-- 3
INSERT INTO therapists (`name`) VALUES ('Robert');
INSERT INTO daily_work_shift (`therapist_id`, `start_time`, `end_time`) VALUES (3,'2022-03-11','00:00:00', '01:00:00');
-- 4
INSERT INTO therapists (`name`) VALUES ('Ervin');
INSERT INTO daily_work_shift (`therapist_id`, `start_time`, `end_time`) VALUES (4,'2022-03-11','05:00:00', '05:30:00');
-- 5
INSERT INTO daily_work_shift (`therapist_id`, `start_time`, `end_time`) VALUES (1,'2022-03-11','21:00:00', '21:45:00');
-- 6
INSERT INTO therapists (`name`) VALUES ('Smith');
INSERT INTO daily_work_shift (`therapist_id`, `start_time`, `end_time`) VALUES (5,'2022-03-11','05:30:00', '05:50:00');
-- 7
INSERT INTO daily_work_shift (`therapist_id`, `start_time`, `end_time`) VALUES (3,'2022-03-11','02:00:00', '02:30:00');

-- C
SELECT `name`
FROM `therapists`
INNER JOIN `daily_work_shift` ON therapists.id = daily_work_shift.therapist_id
ORDER BY target_date, start_time ASC

SELECT `name`
FROM `therapists`
INNER JOIN `daily_work_shift` ON therapists.id = daily_work_shift.therapist_id
ORDER BY
(CASE
	WHEN start_time <= "05:59:59" && start_time >= "00:00:00"
 	THEN (SELECT DATE_ADD(target_date, INTERVAL -1 DAY) AS sort_start_time)
	ELSE (SELECT CONCAT(target_date, " ", start_time) AS sort_start_time)
END), target_date, start_time ASC