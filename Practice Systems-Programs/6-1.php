<?php
require_once('./database.php');

$sql = "SELECT * FROM questions ORDER BY RAND()";
if($conn){
    $result = $conn->query($sql);
}

$data = [];
while($row = $result->fetch_assoc()){
    $data[] = $row;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .questionItem {
            border: 2px solid grey;
            border-radius: 15px;
            padding: 10px;
            margin: 15px;
        }
    </style>
</head>
<body>
    <?php require_once('./6-3.php'); ?>
    <h1 style="text-align: center;">6-1</h1>
    <h1 style="text-align: center;">QUIZ</h1>
    <form action="score.php" method="post">
    <?php
    foreach($data as $key=>$value){
        echo "<div class='questionItem'>";
        echo "<p>";
        echo $key + 1 . ".) " . $value['question'] . "?";
        $sql = "SELECT * FROM choices WHERE question_id='".$value['id']."' ORDER BY RAND() ";
        $choices = [];
        if($result = $conn->query($sql)){
            while($row = $result->fetch_assoc()){
                $choices[] = $row;
            }
        }
        foreach($choices as $choice){
            echo "<br> <input type='radio' name='".$value['id']."' value='".$choice['id']."'>".$choice['name'];
        }
        echo "</p>";
        echo "</div>";
    }
    ?>
    <button type="submit" style="float: right; background-color: #34d13a; color: white; font-weight: bold; ">Submit</button> <br><br>
    </form>
</body>
</html>