<?php

$url = explode("/", $_SERVER['REQUEST_URI']);
$currentFolder = array_slice($url, -2);
if($currentFolder[0] == '3-5'){
    $newUrl = "../";
    $folder = '';
} else {
    $newUrl = '';
    $folder = '../database/3-5/';
}

?>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href=<?php echo $newUrl . "../Practice%20Systems-Programs/6-3.php" ?> >6-3 Navbar</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        HTML and PHP
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-1.php" ?> >1-1</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-2.php" ?> >1-2</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-3.php" ?> >1-3</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-4.php" ?> >1-4</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-5.php" ?> >1-5</a> </li>
                        <li> <hr class="dropdown-divider"> </li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-6_1.php" ?> >1-6</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-7_1.php" ?> >1-7</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-8_1.php" ?> >1-8</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-9.php" ?> >1-9</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-10_and_11_1.php" ?> >1-10 and 1-11</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-12.php" ?> >1-12</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../HTML%20and%20PHP/1-13.php" ?> >1-13</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        JavaScript
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-1.php" ?> >2-1</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-2.php" ?> >2-2</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-3.php" ?> >2-3</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-4.php" ?> >2-4</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-5.php" ?> >2-5</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-6.php" ?> >2-6</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-7.php" ?> >2-7</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-8.php" ?> >2-8</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-9.php" ?> >2-9</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-10.php" ?> >2-10</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-11.php" ?> >2-11</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-12.php" ?> >2-12</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-13.php" ?> >2-13</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-14.php" ?> >2-14</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../JavaScript/2-15.php" ?> >2-15</a></li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Database
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li> <a class="dropdown-item" href=<?php echo $folder . "3-5_1-6-7-8.php" ?> >3-5 of 1-6,7,8</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $folder . "3-5_1-9-12.php" ?> >3-5 of 1-9,10,11,12</a> </li>
                        <li> <a class="dropdown-item" href=<?php echo $folder . "3-5_1-13.php" ?> >3-5 of 1-13</a> </li>
                    </ul>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Practice Systems
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../Practice%20Systems-Programs/6-1.php" ?> >6-1</a></li>
                        <li> <a class="dropdown-item" href=<?php echo $newUrl . "../Practice%20Systems-Programs/6-2.php" ?> >6-2</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>