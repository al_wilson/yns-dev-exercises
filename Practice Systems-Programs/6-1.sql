CREATE TABLE IF NOT EXISTS questions (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    question TEXT
);

INSERT INTO `questions` (`question`)
VALUES
('Who is the 5th Hokage of Konoha'),
('Which one is Naruto''s Jinchuuriki'),
('What is the name of gaara''s village'),
('Who is the leader of AKATSUKI'),
('Which of the legendary sage was Naruto''s mentor'),
('Naruto''s Team number'),
('Which Clan posses Sharingan'),
('Which clan does naruto belong''s to'),
('Which clan does use insects'),
('Which clan does use shadows');

CREATE TABLE IF NOT EXISTS choices (
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    question_id INT NOT NULL,
    name VARCHAR(255),
    FOREIGN KEY (question_id) REFERENCES questions(id)
);

INSERT INTO `choices` (`question_id`, `name`)
VALUES
(1,'Kakashi'),
(1,'Naruto'),
(1,'Tsunade'),
(2, 'Shukaku-Ichibi'),
(2, 'Isobu-Sanbi'),
(2, 'Kurama-Kyuubi'),
(3, 'Konohagakure'),
(3, 'Sunagakure'),
(3, 'Kumogakure'),
(4, 'Nagato'),
(4, 'Itachi'),
(4, 'Orochimaru'),
(5, 'Orochimaru'),
(5, 'Tsunade'),
(5, 'Jiraiya'),
(6, '1'),
(6, '5'),
(6, '7'),
(7, 'Aburame'),
(7, 'Hyūga'),
(7, 'Uchiha'),
(8, 'Hyūga'),
(8, 'Aburame'),
(8, 'Uzumaki'),
(9, 'Nara'),
(9, 'Aburame'),
(9, 'Sarutobi'),
(10, 'Akimichi'),
(10, 'Hyūga'),
(10, 'Nara');

CREATE TABLE IF NOT EXISTS question_answer (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    question_id INTEGER NOT NULL,
    choice_id INTEGER NOT NULL,
    FOREIGN KEY (question_id) REFERENCES questions(id),
    FOREIGN KEY (choice_id) REFERENCES choices(id)
);

INSERT INTO `question_answer` (`question_id`, `choice_id`)
VALUES
(1, 3),
(2, 6),
(3, 8),
(4, 10),
(5, 15),
(6, 18),
(7, 21),
(8, 24),
(9, 26),
(10, 30);